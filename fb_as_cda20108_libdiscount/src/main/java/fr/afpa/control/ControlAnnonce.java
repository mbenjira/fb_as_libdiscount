package fr.afpa.control;

import fr.afpa.metier.ServiceAnnonce;

/**
 * 
 * @author Adrien SOUDE && Fouad BOUCHLAGHEM
 *
 */
public class ControlAnnonce {

	private ServiceAnnonce sa = new ServiceAnnonce();

	public void addAnnonce(String titre, String isbn, String maisonEdition, double prixUni, int quantite, double remise,
			String dateEdition,int idUser) {

		sa.addAnnonce(titre, isbn, maisonEdition, prixUni, quantite, remise, dateEdition, idUser);

	}

}
