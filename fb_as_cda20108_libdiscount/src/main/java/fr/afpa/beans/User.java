package fr.afpa.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class User {

	private int idUser;
	private String nom;
	private String prenom;
	private String mail;
	private String tel;
	private Adresse uneAdresse;

}
