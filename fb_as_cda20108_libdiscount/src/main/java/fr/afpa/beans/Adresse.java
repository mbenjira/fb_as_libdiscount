package fr.afpa.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString

/**
 * 
 * @author Adrien SOUDE && Fouad BOUCHLAGHEM
 *
 */
public class Adresse {

	private int idAdresse;
	private int numRue;
	private String nomRue;
	private String codeP;
	private String ville;

}
