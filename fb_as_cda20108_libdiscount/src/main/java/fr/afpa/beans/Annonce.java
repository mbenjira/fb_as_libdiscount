package fr.afpa.beans;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Annonce {

	
	private int idAnnonce;
	private String titre;
	private String isbn;
	private LocalDate dateEdition;
	private String maisonEdition;
	private double prixUni;
	private int quantite;
	private double remise;
	private LocalDate dateAnnonce;
	private int idUser; 
	

}
