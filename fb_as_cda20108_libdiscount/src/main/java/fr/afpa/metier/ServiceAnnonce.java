package fr.afpa.metier;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.User;
import fr.afpa.dao.DaoAnnonce;

/**
 * 
 * @author Adrien
 *
 */
public class ServiceAnnonce {

	private DaoAnnonce daoA = new DaoAnnonce();
	

	/**
	 * 
	 * Method for create a annonce , and store it in the db 
	 * 
	 * @param titre
	 * @param isbn
	 * @param maisonEdition
	 * @param prixUni
	 * @param quantite
	 * @param remise
	 * @param dateEdition
	 * @param user
	 */
	public void addAnnonce(String titre, String isbn, String maisonEdition, double prixUni, int quantite, double remise,
			String dateEdition,int idUser) {
		
		Annonce annonce = new Annonce();
		annonce.setTitre(titre);
		annonce.setIsbn(isbn);
		annonce.setMaisonEdition(maisonEdition);
		annonce.setDateEdition(LocalDate.parse(dateEdition, DateTimeFormatter.ofPattern("dd-MM-yyyy")));
		annonce.setQuantite(quantite);
		annonce.setRemise(remise);
		annonce.setPrixUni(prixUni);
		calculPrixTotal(annonce);
		
		daoA.addAnnonce(annonce, idUser);
		System.out.println(annonce);
		
	}
	
	/**
	 * Method calcul the total price  of the 'annonce' 
	 * @param annonce
	 */
	public double calculPrixTotal(Annonce annonce) {
		
	return (annonce.getPrixUni()*(1-annonce.getRemise()/100))*annonce.getQuantite();
		
	}

}
