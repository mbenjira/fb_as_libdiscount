package fr.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DaoUser {

	DbConn conn = new DbConn();

	private boolean verifAdresse = false;

	public boolean ajoutAdresse(int i, String string5, String string6, String string7) {

		Connection co = null;
		Statement stmt = null;

		try {

			Class.forName("org.postgresql.Driver");

			co = conn.connect();
			co.setAutoCommit(false);

			stmt = co.createStatement();

			String sql1 = "insert into testadresse (nom_rue, num_rue, ville, code_postal) " + "values ('" + string5
					+ "'," + i + ", '" + string7 + "', '" + string6 + "');";

			stmt.executeUpdate(sql1);

			stmt.close();
			co.commit();
			co.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + " : " + e.getMessage());
			System.exit(0);
		}

		verifAdresse = true;
		return verifAdresse;
	}

	public void ajoutUser(String string, String string2, String string3, String string4, String string8, int i,
			String string5, String string6, String string7) {

		Connection co = null;
		Statement stmt = null;

		try {

			Class.forName("org.postgresql.Driver");

			co = conn.connect();
			co.setAutoCommit(false);

			System.out.println("Base de donn�e bien ouverte !");

			stmt = co.createStatement();

			String sql2 = "insert into testusers (nom, prenom, mail, tel, nomLib, is_active, id_adresse) " + "values ('"
					+ string + "', '" + string2 + "', '" + string3 + "', '" + string4 + "', '" + string8
					+ "' , true, (select id_adresse from testadresse where nom_rue = '" + string5 + "'and ville = '"
					+ string7 + "'and code_postal = '" + string6 + "'));";

			stmt.executeUpdate(sql2);

			stmt.close();
			co.commit();
			co.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + " : " + e.getMessage());
			System.exit(0);
		}

		System.out.println("Utilisateur bien ajout� !");

	}

	/**
	 * Methode for authentification of a user
	 * 
	 * @param login
	 * @param mdp
	 * @return int : -1 if no valid, -2 if account is enable , id_user if it's valid
	 */
	public int auth(String login, String mdp) {

		PreparedStatement stmt = null;
		try {

			String query = "Select check_Login(?,?) ";

			stmt = conn.connect().prepareStatement(query);

			stmt.setString(1, login);
			stmt.setString(2, mdp);

			ResultSet res = stmt.executeQuery();

			if (res.next()) {

				return res.getInt(1);
			}

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (stmt != null) {

				try {

					stmt.close();

				} catch (SQLException e) {

					e.printStackTrace();
				}
			}
		}

		return -1;

	}

}
