package fr.afpa.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.beans.Annonce;

public class DaoAnnonce {

	private DbConn conn = new DbConn();

	public void addAnnonce(Annonce annonce, int idUser) {

		PreparedStatement stmt = null;

		String query = "Insert into Annonce (titre_annonce , isbn ,  date_edition , maison_edition,"
				+ " prix_untaire, quantite,remise, prix_total, id_user) " + "	values(?,?,?,?,?,?,?,?,?)";

		try {

			stmt = conn.connect().prepareStatement(query);

			stmt.setString(1, annonce.getTitre());
			stmt.setString(2, annonce.getIsbn());
			stmt.setDate(3, Date.valueOf(annonce.getDateEdition()));
			stmt.setString(4, annonce.getMaisonEdition());
			stmt.setDouble(5, annonce.getPrixUni());
			stmt.setInt(6, annonce.getQuantite());
			stmt.setDouble(7, annonce.getRemise());

			int r = stmt.executeUpdate();

			System.out.println(r + "nb lignes ajouter ");

		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	public void updateAnnonce(Annonce annonce) {

		PreparedStatement stmt = null;

		String query = "Update Annonce  set titre_annonce  = ? , isbn = ? ,  date_edition = ? , maison_edition = ?,"
				+ " prix_untaire = ? , quantite = ? , remise = ?, where id_annonce = ?";

		try {

			stmt = conn.connect().prepareStatement(query);

			stmt.setString(1, annonce.getTitre());
			stmt.setString(2, annonce.getIsbn());
			stmt.setDate(3, Date.valueOf(annonce.getDateEdition()));
			stmt.setString(4, annonce.getMaisonEdition());
			stmt.setDouble(5, annonce.getPrixUni());
			stmt.setInt(6, annonce.getQuantite());
			stmt.setDouble(7, annonce.getRemise());
			stmt.setInt(8, annonce.getIdAnnonce());

			int r = stmt.executeUpdate();

			System.out.println(r + "nb lignes modifier ");

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			if (stmt != null) {

				try {

					stmt.close();

				} catch (SQLException e) {

					e.printStackTrace();
				}
			}
		}

	}

	public void deleteAnnonce(Annonce annonce) {

		PreparedStatement stmt = null;

		String query = "Delete from Annonce where id_annonce = ? ";

		try {

			stmt = conn.connect().prepareStatement(query);
			stmt.setInt(1, annonce.getIdAnnonce());

			int r = stmt.executeUpdate();

			System.out.println(r + "Lignes supprimer ");
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	public List<Annonce> ListerAnnonce(){
		
		ArrayList<Annonce> listAnnonce = new ArrayList<>();
		PreparedStatement stmt = null;
		
			String query = "Select * from Annonce";
			
			try {
				
				stmt = conn.connect().prepareStatement(query);
				ResultSet res = stmt.executeQuery();
				
				Annonce annonce = null; 
				
				while (res.next()) {
					

					annonce = new Annonce();
					annonce.setIdAnnonce(res.getInt(1));
					annonce.setTitre(res.getString(2));
					annonce.setIsbn(res.getString(3));
					annonce.setDateEdition(LocalDate.parse(res.getString(4)));
					annonce.setPrixUni(res.getDouble(6));
					annonce.setMaisonEdition(res.getString(5));
					annonce.setQuantite(res.getInt(7));
					annonce.setRemise(res.getDouble(8));
					annonce.setDateAnnonce(LocalDate.parse(res.getString(9)));
					annonce.setIdUser(res.getInt(10));

					listAnnonce.add(annonce);
				
				}
				
				
			} catch (SQLException e) {

				e.printStackTrace();
			}
			
		return listAnnonce;
		
	}

}
