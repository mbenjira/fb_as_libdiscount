package fr.afpa.vue;

import java.util.Scanner;

import fr.afpa.beans.User;
import fr.afpa.control.ControlAnnonce;
import fr.afpa.control.ControlSaisie;
import fr.afpa.control.ControlUser;

/**
 * 
 * @author Adrien SOUDE && Fouad BOUCHLAGHEM
 *
 */

public class AffichageMenu {

	private ControlAnnonce ca = new ControlAnnonce();
	private ControlSaisie cs = new ControlSaisie();
	private ControlUser cu = new ControlUser();

	Scanner scan = new Scanner(System.in);

	/**
	 * Method for print the start Menu
	 */
	public void startMenu() {

		System.out.println("-----------------Bienvenu----------------");
		System.out.println("-       Tapez 1 pour vous inscrire      -");
		System.out.println("-                                       -");
		System.out.println("-       Tapez 2 pour vous connecter     -");
		System.out.println("-                                       -");
		System.out.println("-       Tapez 3 pour quitter            -");
		System.out.println("-----------------------------------------");

		startMenu(scan);

	}

	/**
	 * Methode for print the add annonce Menu
	 */
	public void addAnnonce(int idUser) {

		System.out.println("-------------- Ajouter Votre Annonce ----------------");
		addAnnonce(scan, idUser);
		System.out.println("-----------------------------------------------------");

	}

	/**
	 * Method for print the authification menu
	 */
	public void authMenu() {

		System.out.println("----------------- Espace Connection -----------------");
		System.out.println("-              Tapez -1 pour quitter                -");
		authMenu(scan);		
		
	}

	public void mainMenu(int userID) {
		System.out.println(userID);
		System.out.println("----------------- Voir les Annonces -----------------");
		System.out.println("-        Tapez 1 pour voir les annonces             -");
		System.out.println("-        Tapez 2 pour rechercher des annonces       -");
		System.out.println("-        Tapez 3 pour voir vos annonces             -");
		System.out.println("-        Tapez 4 pour ajouter une annoces           -");
		System.out.println("-        Tapez 5 pour constulter vos infos          -");
		System.out.println("-        Tapez 6 pour quitter                       -");
		System.out.println("-----------------------------------------------------");
		gestionMainMenu(scan, userID);
	}

	/**
	 * methode de gestion du menu de connexion
	 * 
	 * @param scan
	 */
	public void startMenu(Scanner scan) {

		int choix = 0;

		while (choix != 3) {

			choix = scan.nextInt();

			switch (choix) {

			case 1:
				System.out.println("Menu Inscription");
				break;
			case 2:
				authMenu();
				break;
			case 3:
				System.exit(0);
				break;
			default:
				System.out.println("Choix invalide, veuillez choisir une option valide ");
				break;
			}

		}

	}
	
	public void gestionMainMenu(Scanner scan, int idUser) {
		int choix = 0;

		while (choix != 6) {

			choix = scan.nextInt();

			switch (choix) {

			case 1:
				System.out.println("voir les annonces");
				break;
			case 2:
				System.out.println("search annonce");
				break;
			case 3:
				System.out.println("my annonces");
				break;
			case 4:
				addAnnonce(idUser);
				break;
			case 5:
				System.out.println("mes infos ");
				break;
			case 6:
				System.exit(0);
				break;
			default:
				System.out.println("Choix invalide, veuillez choisir une option valide ");
				break;
			}

		}
	}

	/**
	 * Form for add annonce
	 * 
	 * @param scan : a Scanner;
	 */
	public void addAnnonce(Scanner scan, int idUser) {

		int choix = 0;
		String titre;
		String isbn;
		String maisonE;
		String dateE;
		double prixUni;
		double remise;
		int quantite;

		do {

			System.out.println("-  Veuillez entrer le titre de votre annonce        -");
			titre = scan.nextLine();

		} while (titre.isEmpty());

		do {

			System.out.println("-  Veuillez entrer l'ISBN du livre � vendre         -");
			isbn = scan.nextLine();

		} while (isbn.isEmpty() || !cs.isNumeric(isbn));

		do {

			System.out.println("-  Veuillez entrer La maison d'�dition du livre     -");
			maisonE = scan.nextLine();

		} while (maisonE.isEmpty());

		do {

			System.out.println("-  Veuillez entrer le prix unitaire du livre        -");
			prixUni = scan.nextDouble();
			scan.nextLine();

		} while (!cs.checkPostiveDouble(prixUni));

		do {

			System.out.println("-  Veuillez entrer la date d'�dition (jj-MM-aaaa)   -");
			dateE = scan.nextLine();

		} while (!dateE.isEmpty() && !cs.isDate(dateE));

		do {

			System.out.println("-  Veuillez entrer la quantite de livres            -");
			quantite = scan.nextInt();
			scan.nextLine();

		} while (!cs.checkPostiveInt(quantite));

		do {

			System.out.println("-  Veuillez entrer la remise sur le prix original   -");
			remise = scan.nextDouble();
			scan.nextLine();

		} while (!cs.checkPostiveDouble(remise));

		User user = new User();

		ca.addAnnonce(titre, isbn, maisonE, prixUni, quantite, remise, dateE, idUser);

		do {

			System.out.println("-            Tapez -1 pour quitter                  -");
			choix = scan.nextInt();
			scan.nextLine();

		} while (choix != -1);

	}

	public void authMenu(Scanner scan) {

		String login = "";
		String mdp = "";

		

		do {

			System.out.println("-              Veuillez entrer votre login          -");
			login = scan.nextLine();

			if (login.equals("-1")) {
				System.out.println("-----------------------------------------------------");
				startMenu();
			}

		} while (login.isEmpty());

		do {

			System.out.println("-              Veuillez entrer votre mot de passe   -");
			mdp = scan.nextLine();
		
			if (mdp.equals("-1")) {
				System.out.println("-----------------------------------------------------");
				startMenu();
			}

		} while (mdp.isEmpty());

		if (cu.auth(login, mdp) > 0) {
			System.out.println("-----------------------------------------------------");
			mainMenu(cu.auth(login, mdp));

		} else if (cu.auth(login, mdp) == -2) {
			System.out.println("Votre compte est d�sactiver..");
			System.out.println("-----------------------------------------------------");
			startMenu();
		} else {
			System.out.println("Mot de passe ou login incorrect ");
			System.out.println("-----------------------------------------------------");
			authMenu();
		}
	}

	public void mainMenu(Scanner scan) {

	}

}
